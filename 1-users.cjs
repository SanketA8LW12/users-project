const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

// Q1 Find all users who are interested in playing video games.

function userIntrestedInPlayingVideoGame(users){
    let userPlayingVideoGame = [];
    Object.entries(users).map((userData) => {
    const[key, value] = userData;
    let intrestArray = value.interests;

    let result = intrestArray.map((element => {
        return element.includes('Video Games');
    }))

    if(result[0]){
        userPlayingVideoGame.push(JSON.stringify(userData));
    }
})
console.log(userPlayingVideoGame);
}
userIntrestedInPlayingVideoGame(users);

console.log("---------------------------------------");

// Q2 Find all users staying in Germany.


function userStayingInGermany(users){
    let userInGermany = [];
    Object.entries(users).map((userData)=>{
        const[key, value] = userData;
        if(value.nationality === 'Germany'){
            userInGermany.push(userData);
        }
    })
    console.log(userInGermany);
}
userStayingInGermany(users);

console.log("---------------------------------------");

// Q4 Find all users with masters Degree.

function userWithMasterDegree(users){
    let userMasterDegree = [];
    Object.entries(users).map((userData)=>{
        const[key, value] = userData;
        if(value.qualification === 'Masters'){
            userMasterDegree.push(userData);
        }
    })
    console.log(userMasterDegree);
}
userWithMasterDegree(users);

// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10